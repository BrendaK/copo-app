import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';

import { environment } from 'src/environments/environment';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  constructor(private httpClient: HttpClient) {
    this.setHeadersBasicOptions();
    this.setHeadersPaginateOptions();
  }

  // HttpHeaders Options
  headersBasic: HttpHeaders | undefined;  
  headersPaginate: HttpHeaders | undefined;  

  setHeadersBasicOptions() {
    this.headersBasic = new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token'),
      'paginate' : '0'
    })
  }  

  setHeadersPaginateOptions() {
    this.headersPaginate = new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token'),
      'paginate' : '1'
    })
  }  
  

  get(url:any) {
    return this.httpClient.get(API_URL+url, {headers:this.headersBasic});
  }

  paginate(url:any) {
    return this.httpClient.get(API_URL+url, {headers:this.headersPaginate});
  }

  filter(url:any, data: any) {
    return this.httpClient.post(API_URL+url, data, {headers:this.headersPaginate});
  }


  post(url:any, data: any) {
    return this.httpClient.post(API_URL + url, data, {headers:this.headersBasic});
  }

  delete(url:any) {
    return this.httpClient.delete(API_URL + url, {headers:this.headersBasic});
  }

  // Error handling 
  handleError(error: any) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }
}
