import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from './../api/api.service';
import { Router } from '@angular/router';

import { environment } from './../../../environments/environment';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private api: ApiService,
    private http: HttpClient,
    private router: Router
  ) { }

  /**
   * this is used to clear anything that needs to be removed
   */
  clear(): void {
    localStorage.clear();
  }

  /**
   * check for expiration and if token is still existing or not
   * @return {boolean}
   */
  id(): any {
    return localStorage.getItem('id');
  }

  /**
   * check for expiration and if token is still existing or not
   * @return {boolean}
   */
  isAuthenticated(): boolean {
    return localStorage.getItem('token') != null;
  }

  /**
   * check for expiration and if token is still existing or not
   * @return {boolean}
   */
  isAdmin(): boolean {
    return (localStorage.getItem('user_type_id') == '1') ? true : false;
  }

  userTypeId() : any {
    return localStorage.getItem('user_type_id');
  }

  login(data: any): void {
    this.http.post(API_URL+'login', data).subscribe((data: any) => {
          localStorage.setItem('id', data.id);
          localStorage.setItem('token', data.token);
          localStorage.setItem('firstname', data.firstname);
          localStorage.setItem('lastname', data.lastname);
          localStorage.setItem('email', data.email);
          localStorage.setItem('user_type_id', data.user_type_id);

          this.api.setHeadersBasicOptions();
          this.api.setHeadersPaginateOptions();
          this.router.navigate(["/private/listes"]);
        // }
      },
      err => {
        alert( "Erreur sur vos codes d'accÃ¨s" );
      }
    );
  }

  /**
   * this is used to clear local storage and also the route to login
   */
  logout(): any {
    this.clear();
    this.router.navigate(['/login']);
  }
}