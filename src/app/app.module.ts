import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { WhoWeAreComponent } from './pages/who-we-are/who-we-are.component';
import { ActualityComponent } from './pages/actuality/actuality.component';
import { JoinUsComponent } from './pages/join-us/join-us.component';
import { ContactComponent } from './pages/contact/contact.component';
import { MarineComponent } from './pages/products/marine/marine.component';
import { TwoStrokeEnginesComponent } from './pages/products/marine/two-stroke-engines/two-stroke-engines.component';
import { FourStrokeEnginesComponent } from './pages/products/marine/four-stroke-engines/four-stroke-engines.component';
import { WaverunnersComponent } from './pages/products/marine/waverunners/waverunners.component';
import { RoadComponent } from './pages/products/road/road.component';
import { MotorcyclesComponent } from './pages/products/road/motorcycles/motorcycles.component';
import { ScootersComponent } from './pages/products/road/scooters/scooters.component';
import { AssComponent } from './pages/products/ass/ass.component';
import { MaintenanceComponent } from './pages/products/ass/maintenance/maintenance.component';
import { DetachedPiecesComponent } from './pages/products/ass/detached-pieces/detached-pieces.component';
import { YamalubeComponent } from './pages/products/ass/yamalube/yamalube.component';
import { IslandToursComponent } from './pages/products/ass/island-tours/island-tours.component';
import { ProMarketComponent } from './pages/products/pro-market/pro-market.component';
import { NavBusinessComponent } from './share/nav-business/nav-business.component';
import { NavProductsComponent } from './share/nav-products/nav-products.component';
import { FooterComponent } from './share/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    WhoWeAreComponent,
    ActualityComponent,
    JoinUsComponent,
    ContactComponent,
    MarineComponent,
    TwoStrokeEnginesComponent,
    FourStrokeEnginesComponent,
    WaverunnersComponent,
    RoadComponent,
    MotorcyclesComponent,
    ScootersComponent,
    AssComponent,
    MaintenanceComponent,
    DetachedPiecesComponent,
    YamalubeComponent,
    IslandToursComponent,
    ProMarketComponent,
    NavBusinessComponent,
    NavProductsComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    // ReactiveFormsModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
