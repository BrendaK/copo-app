import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-detached-pieces',
  templateUrl: './detached-pieces.component.html',
  styleUrls: ['./detached-pieces.component.scss']
})
export class DetachedPiecesComponent implements OnInit {

  nameProduct: string = "XXX";
  imageProduct: string = "XXX";
  descriptionProduct: string = "XXX";

  constructor(
    private api: ApiService,
  ) { }

  ngOnInit(): void {
    this.getProduct(1);
  }

  getProduct(id: number) {
    this.api.get("sav/" + id).subscribe((data: any) => {
      this.nameProduct = data.name;
      this.imageProduct = data.image;
      this.descriptionProduct = data.description;
    }, (err: {error: string}) => {
      console.log(err.error);
    });
  }

}
