import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-motorcycles',
  templateUrl: './motorcycles.component.html',
  styleUrls: ['./motorcycles.component.scss']
})
export class MotorcyclesComponent implements OnInit {

  motos: any; 
  moto: any;
  id: number;

  constructor(
    private api : ApiService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
  ) { 
    this.id =  this.activatedRouter.snapshot.params.id;
  }

  ngOnInit(): void {
    this.getAllMotos();
  }

  getAllMotos(){
    this.api.get('motos').subscribe((data) => {
      this.motos = data;
      console.log(data);
    })
  }

  filterById(id: number){
    this.api.get('motos/' +id).subscribe((data) => {
      this.moto = data;
      console.log("moto", data);
      })
  }
}
