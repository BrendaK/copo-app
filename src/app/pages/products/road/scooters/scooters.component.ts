import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-scooters',
  templateUrl: './scooters.component.html',
  styleUrls: ['./scooters.component.scss']
})
export class ScootersComponent implements OnInit {

  scooters: any;


  constructor(
    private api : ApiService,
    private router: Router,
    // private activatedRouter: ActivatedRoute,
    ) {
      // this.id =  this.activatedRouter.snapshot.params.id;
     }

  ngOnInit(): void {
    this.getAllScooters();
  }

  getAllScooters(){
    this.api.get('scooters').subscribe((data) => {
      this.scooters = data;
      console.log(data);
    })
  }
}
