import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { ActualityComponent } from './pages/actuality/actuality.component';
import { ContactComponent } from './pages/contact/contact.component';
import { JoinUsComponent } from './pages/join-us/join-us.component';
import { AssComponent } from './pages/products/ass/ass.component';
import { DetachedPiecesComponent } from './pages/products/ass/detached-pieces/detached-pieces.component';
import { IslandToursComponent } from './pages/products/ass/island-tours/island-tours.component';
import { MaintenanceComponent } from './pages/products/ass/maintenance/maintenance.component';
import { YamalubeComponent } from './pages/products/ass/yamalube/yamalube.component';
import { FourStrokeEnginesComponent } from './pages/products/marine/four-stroke-engines/four-stroke-engines.component';
import { TwoStrokeEnginesComponent } from './pages/products/marine/two-stroke-engines/two-stroke-engines.component';
import { WaverunnersComponent } from './pages/products/marine/waverunners/waverunners.component';
import { ProMarketComponent } from './pages/products/pro-market/pro-market.component';
import { MotorcyclesComponent } from './pages/products/road/motorcycles/motorcycles.component';
import { ScootersComponent } from './pages/products/road/scooters/scooters.component';
import { WhoWeAreComponent } from './pages/who-we-are/who-we-are.component';

const routes: Routes = [
  {path:"", redirectTo:"accueil", pathMatch:"full"},
  {path: "accueil", component: AccueilComponent},
  {path: "actuality", component: ActualityComponent},
  {path: "contact", component: ContactComponent},
  {path: "join-us", component: JoinUsComponent},
  {path: "who-we-are", component: WhoWeAreComponent},
  {path: "pro-market", component: ProMarketComponent},
  {path: "ass", children : [
    {path: "detached-pieces", component: DetachedPiecesComponent},
    {path: "island-tours", component: IslandToursComponent},
    {path: "maintenance", component: MaintenanceComponent},
    {path: "yamalube", component: YamalubeComponent},
  ]},
  {path: "marine", children : [
    {path: "four-stroke-engines", component: FourStrokeEnginesComponent},
    {path: "two-stroke-engines", component: TwoStrokeEnginesComponent},
    {path: "waverunners", component: WaverunnersComponent},
  ]},
  {path: "road", children : [
    {path: "motorcycles", component: MotorcyclesComponent},
    {path: "scooters", component: ScootersComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
